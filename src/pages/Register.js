import {Form, Button} from 'react-bootstrap';
import {useState, useEffect, useContext} from 'react';
import {Navigate,useNavigate} from 'react-router-dom';
import UserContext from '../UserContext.js';
import Swal from 'sweetalert2'

export default function Register(){

	const navigate = useNavigate();
	const {user} = useContext(UserContext);

	// State Hooks -> store values of the input fields
	const [firstName, setFirstName] = useState('');
	 const [lastName, setLastName] = useState('');
	 const [mobileNum, setMobileNum] = useState('');
	const [email, setEmail] = useState('');
	const [password1, setPassword1] = useState('');
	const [password2, setPassword2] = useState('');

	const[isActive, setIsActive] = useState(false);

	console.log(email);
	console.log(password1);
	console.log(password2);

	useEffect(() => {

		if((firstName!== '' && lastName !== '' && mobileNum !== '' && email !== '' && password1 !== '' && password2 !== '') && (password1 === password2)){
			setIsActive(true);
		}else {
			setIsActive(false);
		}
	}, [firstName, lastName, mobileNum, email, password1, password2]);


	//Simulate user registration

	function registerUser(e) {
		//prevents page redirection via form submission
		e.preventDefault();

		fetch('http://localhost:4000/users/checkEmail', {
			method: 'POST',
			body: JSON.stringify({email}),
			headers: {
				'Content-Type': 'application/json'
			}
		})
		.then(res => res.json())
		.then(data => {
			
			if(data === true){
					Swal.fire({
					title: "Duplicate Email Found!",
					icon: "error",
					text: "Please try another Email!"
				})
			}else{
				
				fetch('http://localhost:4000/users/register', {
				method: 'POST',
				headers: {
					'Content-Type': 'application/json'
				},
					body: JSON.stringify({
						firstName : firstName,
						lastName: lastName,
						email: email,
						mobileNo: mobileNum,
						password: password1
					})
				})
				.then(res => res.json())
				.then(data => {
					// console.log(data)

					Swal.fire({
						title: 'Successfully Registered',
						icon: 'success',
						text: 'Welcome to Zuitt!'
					})

					navigate('/login');
		    	})
				
			}
		})

		setFirstName('');
		setLastName('');
		setMobileNum('');
		setEmail('')
		setPassword1('');
		setPassword2('');
	}


	return(
		 (user.id !== null)?

		<Navigate to="/courses" />
		:
	<Form onSubmit={(e) => registerUser(e) }>
		{/*FirstName*/}
			<Form.Group className="mb-3" controlId="userEmail">
        <Form.Label>First Name</Form.Label>
        <Form.Control type="text" placeholder="Enter firstname" value={firstName} onChange={e => setFirstName(e.target.value)} required/>
      </Form.Group>

		{/*Lastname*/}
		<Form.Group className="mb-3" controlId="userEmail">
        <Form.Label>Last Name</Form.Label>
        <Form.Control type="text" placeholder="Enter lastname" value={lastName} onChange={e => setLastName(e.target.value)} required/>
      </Form.Group>

    {/*Mobile Number*/}
    <Form.Group className="mb-3" controlId="userEmail">
        <Form.Label>Mobile Number</Form.Label>
        <Form.Control type="number" placeholder="Enter mobile number" value={mobileNum} onChange={e => setMobileNum(e.target.value)} required/>
      </Form.Group>
    {/*Email Address*/}
      <Form.Group className="mb-3" controlId="userEmail">
        <Form.Label>Email address</Form.Label>
        <Form.Control type="email" placeholder="Enter email" value={email} onChange={e => setEmail(e.target.value)} required/>
        <Form.Text className="text-muted">
          We'll never share your email with anyone else.
        </Form.Text>
      </Form.Group>

      {/*password1*/}
      <Form.Group className="mb-3" controlId="password1">
        <Form.Label>Password</Form.Label>
        <Form.Control type="password" placeholder="Password" value={password1} onChange={e => setPassword1(e.target.value)} required/>
      </Form.Group>

  	  {/*password2*/}
      <Form.Group className="mb-3" controlId="password2">
        <Form.Label>Verify Password</Form.Label>
        <Form.Control type="password" placeholder="Verify Password" value={password2} onChange={e => setPassword2(e.target.value)} required/>
      </Form.Group>
  		{/*CONDITIONAL RENDERING -> IF ACTIVE IS CLICKABLE -> IF INACTIVE BUTTON IS NOT CLICK, ?-means if and : - means else*/}
      {
      	(isActive) ?
      <Button variant="primary" type="submit" controlId="submitBtn">
        Register
      </Button>
      :
      <Button variant="primary" type="submit" controlId="submitBtn" disabled>
        Register
      </Button>
      }
      
    </Form>

		)
}