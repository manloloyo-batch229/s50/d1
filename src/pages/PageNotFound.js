//import React from 'react';
//import {Link} from 'react-router-dom';
import Banner from '../components/Banner.js';


export default function PageNotFound(){

		const data = {
			title: "404 Page Not Found",
			content: "The page you are looking for cannot be found",
			destination: "/",
			label: "Back Home"
		}
	return(
		
		<Banner data= {data} />

		)
}